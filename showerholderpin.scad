
PI = 3.1415;
L = 82;
S = 70;
H1 = 45;
R1 = L / (2 * PI);
R2 = S / (2 * PI); 
R3 = 5;
H2 = 20;
cylinder(r1=R2, r2=R1, h=H1);
translate([0, 0, H1]){
    cylinder(r=R3, h=H2);
}
